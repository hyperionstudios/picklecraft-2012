<?php
$head = "<style type=\"text/css\">
body {
    background: #68707E;    
}
#banlist {
    color: #FFFFFF;
    width: 100%;
}
.ban_player {
   color:red;
   font-size:20px;
   font-weight:bold;
   padding-left:0.3em; 
}
.op_banner {
    color:#4DFF00;
    font-size:20px;
    font-weight:bold;
    padding-left:0.3em;
}
</style>
<table id=\"banlist\">";
$foot = "</table>";

error_reporting(E_ALL);
ini_set('display_errors','On');
function parser() {
    $fh = fopen("bans","r");
    $l = array();
    while (($data = fgetcsv($fh,1024,",")) == true) {        
        $a = array();
        $c = count($data);
        $d = "<tr>";
        for ($i = 0; $i < $c; $i++) {
            if (!empty($data[$i])) {
                //Player banned
                if ($i == 0) {
                    $d .= "<td class='ban_player'>$data[$i]</td>";
                }
                //reason and operator ("reason (oper)")
                else if ($i == 2) {
                    $v = preg_split("/[\((+?)\)]/",$data[$i]);
                    $d .= "<td>$v[0]</td>"; //Reason
                    //Operator that banned
                    if (!empty($v[1])) {
                        $d .= "<td class='op_banner'>$v[1]</td>";
                    }
                    else {
                         $d .= "<td>N/A</td>";
                    }
                }
                //Date
                else if ($i == 3) {
                    $d .= "<td>". @date("F j, Y, g:i a",$data[$i]/1000)."</td>";
                    $a[1] = $data[$i];
                }
                //Additonal stuff
                else {
                     //$d .= "<td>".$data[$i]."</td>";
                }
            }
        }
        $d .= "</tr>";
        $a[0] = $d;
        array_push($l,$a);
    }

    fclose($fh);
    usort($l,"derp");
    
    cache($l);
}
function derp($a,$b) {
    return $a[1] < $b[1];
}

function cache($array) {
    global $head,$foot;
    $file = "bans.html";
    $fh = fopen($file,"w");
    fwrite($fh,$head);
    $c = count($array);
    for ($i = 0; $i < $c; $i++) {
        fwrite($fh,$array[$i][0]);
        fwrite($fh,"\n");
    }
    fwrite($fh,$foot);
    fclose($fh);
}

function readCache() {
    $file = "bans.html";
    $new = false;
    if (!file_exists($file)) {
        $fh = fopen($file,"w");
        fwrite($fh,"");
        fclose($fh);
        $new = true;
    }
    if (filemtime($file) + 300 <= time() || $new) {
        parser();
    }
         
     $fh = fopen($file,"r");
     while (($data = fread($fh,1024)) == true) {
        print $data;
     }
     fclose($fh);
}

readCache();
?>
