<html>
    <head>
        <title>Rules</title>
        <style type='text/css'>
            body {
                color:#247488;
                background: #DDDCE4;
            }
            .small {
                font-size: small;
                margin: 0 0 0 0.5em;
            }
            .content {
                text-align: center;
            }
            ol {
                margin:auto;
                text-align:left;
                width:60%;
            }
        </style>
    </head>
    <body>
        <div class='content'>
            <h1>Rules</h1>
            <a href='http://forum.solar-storm.net'>Return back to forums</a>
            <ol>
                <li>
                    <strong>No griefing.</strong>
                    <div class='small'>
                       This includes, building to close to structures with intention of disrupting the original build.
                    </div>
                </li>
                <li>
                    <strong>No hacks/mods of any kind.</strong>
                    <div class='small'>
                        Mods that gives an advantage over the vanilla client is prohibited.<br />
                        &nbsp;eg.
                        &nbsp;Minimaps that does not display player locations is allowed.
                        &nbsp;Xray is not.
                    </div>
                </li>
                <li>
                    <strong>No harrasment of players.</strong>
                    <div class='small'>
                        This includes, building racist structures eg. swatiskas.
                    </div>
                </li>
                <li>
                    <strong>No spamming or advertising.</strong>
                </li>
                <li>
                    <strong>Pvp is allowed.</strong>
                    <div class='small'>
                        If you die from &quot;unfair pvp&quot; and lose your items. Then tough luck, go kill them back.<br />
                        However teleport killing is prohibited.<br/>
                        /jumpto, /thru, and compass jumping is teleporting.<br />
                        However...pvp after accepting a /tpa is allowed since the other player has to accepts it and agrees to the risk.<br />
                    </div>
                 </li>
                 <li>
                   <strong>Setting your home in someone's else home unless given permission from owner is also prohibited.</strong>
                 </li>
                 <li>
                    <strong>No permanment 1x1 towers, dirt huts, or large stuctures with 90% cobble.</strong>
                    <div class='small'>
                        They will be destroyed and a warning shall be received.
                    </div>
                </li>
            </ol>
            <br />
            <div>
                first offence = Ban with ability to appeal and liklihood for unban is great.<br />
                second offence =  Ban with ability to appeal and liklihood for unban depends on offence and former offence.<br />
                third offence =  Appeal will most likly be denied.<br />
            </div>
            These rules are not 100% set in stone at this moment, and are to be changed at any time.
        </div>
    </body>
</html>