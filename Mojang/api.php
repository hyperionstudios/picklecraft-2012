       <?php
            function get_status_json() {
                $cachefile = 'cache.json';
                $cachetime = 1;
                if (file_exists($cachefile) && time() - $cachetime < filemtime($cachefile)) {
                    $result = file_get_contents($cachefile);
                } else {            
                    $url = 'http://status.mojang.com/check';
                    $ch = curl_init($url);
                    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.101 Safari/537.11');
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 5);
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
                    $result = curl_exec($ch);
                    $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                    curl_close($ch);
                    
                    if ($http_code == 200) {
                    	@chmod('cache.json');
                        file_put_contents($cachefile, $result);
                        echo $result;
                    } else {
                        echo '<ul>';
                            echo '<li><span class="status offline">&middot;</span>MOJANG </li>';
                        echo '</ul>';
                    }
                }
                
                return $result;
            }
            
            $messages = array();
            $config = explode("\n", file_get_contents('config/messages.txt'));
            $service = '';
            foreach ($config as $line) {
                if (substr($line, 0, 1) !== '#') {
                    if (preg_match('#\[([a-z]+)\]#i', $line, $matches)) {
                        $service = $matches[1];
                    } else {
                        $messages[$service][] = $line;
                    }
                }
            }
            
            $_json = get_status_json();
            file_put_contents('cache.json', $_json);
            $_json = json_decode($_json, true);
            
            if (count($_json) > 0) {
                $size = count($_json);
                for ($i = 0; $i < $size; $i++) {
                    $site = key($_json[$i]);
                    $json[$site] = $_json[$i][$site];
                }
            
                echo '<center><ul>';
                    echo '<li><span class="status ' . ($json['session.minecraft.net'] =='green' ? 'online' : 'offline') . '">&middot;</span>SESSIONS</li>';
                    echo '<li><span class="status ' . ($json['login.minecraft.net'] == 'green' ? 'online' : 'offline') . '">&middot;</span>LOGIN</li>';
                    echo '<li><span class="status ' . ($json['skins.minecraft.net'] == 'green' ? 'online' : 'offline') . '">&middot;</span>SKINS</li>';
                    echo '<li><span class="status ' . ($json['auth.mojang.com'] == 'green' ? 'online' : 'offline') . '">&middot;</span>ACCOUNTS</li>';
                    echo '<li><span class="status ' . ($json['minecraft.net'] == 'green' ? 'online' : 'offline') . '">&middot;</span>WEBSITE</li>';
                echo '</ul></center>';
                echo '<div class="clearfix"></div>';
                
                if ($json['session.minecraft.net'] == 'red') {
                    echo '<div class="outage major">' . $messages['sessions'][0] . '</div>';
                } else if ($json['login.minecraft.net'] == 'red') {
                    echo '<div class="outage major">' . $messages['login'][0] . '</div>';
                } else if ($json['skins.minecraft.net'] == 'red') {
                    echo '<div class="outage major">' . $messages['skins'][0] . '</div>';
                } else if ($json['auth.mojang.com'] == 'red') {
                    echo '<div class="outage major">' . $messages['auth'][0] . '</div>';
				} else if ($json['minecraft.net'] == 'red') {
                    echo '<div class="outage major">' . $messages['website'][0] . '</div>';
                }
            }
            
            // Please don't remove the line below, unless permission was given by the author (Jeffrey Angenent, Artaex Media).
            //echo base64_decode('PGRpdiBjbGFzcz0iYXV0aG9yIj5EZXZlbG9wZWQgYnkgPGEgaHJlZj0iaHR0cDovL2Rlc2lnbi5hcnRhZXguY29tIiB0YXJnZXQ9Il9ibGFuayI+QXJ0YWV4IE1lZGlhPC9hPjwvZGl2Pg==');
            // ***************************************************************************************************************
        ?>