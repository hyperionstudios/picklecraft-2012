# Strings should be seperated by a newline
# Lines starting with # are ignored
# Please don't remove these lines: "[servicename]" otherwise messages will mix up
# And make sure you don't add any extra empty lines
[session]
Multiplayer sessions down
[login]
Login server down
[skins]
Steve's everywhere!
[auth]
account.mojang.com is down
[website]
Minecraft website is offline