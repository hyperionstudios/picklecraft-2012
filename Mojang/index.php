<?php
	if (!in_array('curl', get_loaded_extensions())) die('cURL is not installed');
    //if (strpos($_SERVER['HTTP_REFERER'], "punchcraft.us") === false) die('Please contact: design@artaex.com');
    
    //header('refresh:30;url=index.phpz');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Mini Status</title>
        <link rel="stylesheet" type="text/css" href="http://twitter.github.com/bootstrap/assets/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="style.css">
		<script src="http://twitter.github.com/bootstrap/assets/js/jquery.js"></script>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
		<meta name="author" content="kittenchunks">
		<meta name="robots" content="noindex,nofollow">
	</head>
<body class="module">
<div id="bar">
<div class="refresh" align="center"> Refreshing in: <span id="countdown"> </span> seconds</div>

<div id="status">

</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
 update();
 var counter=setInterval(timer, 1000);
 setInterval(update, 30000);
});
var count=30;
 function timer() {
  count=count-1;
    if (count <= 0) {
       count = 30;
    }
                document.getElementById("countdown").innerHTML = count;
            }
 function update() {
  $("#status").fadeOut('slow');
  $("#status").load("api.php").fadeIn('slow');
 }
</script>
</body>
</html>