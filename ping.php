<?php

 /** Modified from barneygale https://gist.github.com/1235274 */
function pingWithExtra($host, $port=25565, $timeout=30) {
    $time = microtime();
    //Set up our socket
    $fp = false;
    if (!$fp = @fsockopen($host, $port, $errno, $errstr, $timeout)) return false;
    $time= ceil((microtime() - $time)*1000);

    //Send 0xFE: Server list ping
    fwrite($fp, "\xFE");

    //Read as much data as we can (max packet size: 241 bytes)
    $d = fread($fp, 256);

    //Check we've got a 0xFF Disconnect
    if (count($d)) {
        if(0 && $d[0] !== "\xFF") return false;
    }
    else {
        return false;
    }

    //Remove the packet ident (0xFF) and the short containing the length of the string
    $d = substr($d, 3);

    //Decode UCS-2 string
    $d = mb_convert_encoding($d, 'auto', 'UCS-2');

    //Split into array
    $d = explode("\xA7", $d);
    $data = count($d);
    //print $data;
    //Return an associative array of values
    $array = array('time' => $time, 'motd' => $d[0], 'players' => "N",'max_players' => "A");
    if ($data >= 2) { $array['players'] = intval($d[1]); }
    if ($data >= 3) { $array['max_players'] = intval($d[2]); }
    return $array;
}

/*
Do actual work
*/

//smp
$ping[0] = pingWithExtra("mc.picklecraft.net",25565,10);
$ping[0]['name'] = "SMP (Main Server)";

//tekkit
$ping[1] = pingWithExtra("mc.picklecraft.net", 25567,10);
$ping[1]['name'] = "Tekkit";

?>
<table>
<?php
    
    $c = count($ping);
    for ($i=0; $i < $c; $i++) {
        $status = $ping[$i]['time'] > 0 ? "online" : "offline";
?>
    <tr>
        <th colspan="2"><?php print $ping[$i]['name']; ?></th>
    </tr>
    <tr>
        <td>Status:</td>
        <td><img src="images/<?php print $status; ?>.png" alt="<?php print $status; ?>"/></td>
    </tr>
    <tr>
        <td>Players:</td>
        <td><?php print $ping[$i]['players'] ."/". $ping[$i]['max_players']; ?></td>
    </tr>
<?php
    }
?>
</table>